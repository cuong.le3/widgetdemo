//
//  IntentHandler.swift
//  WidgetIntents
//
//  Created by Hoang Cuong on 10/22/21.
//

import Intents

class IntentHandler: INExtension, SelectLocationIntentHandling {
    func resolveLocation(for intent: SelectLocationIntent, with completion: @escaping (LocationResolutionResult) -> Void) {
        completion(.notRequired())
    }
    
    func provideLocationOptionsCollection(for intent: SelectLocationIntent, with completion: @escaping (INObjectCollection<Location>?, Error?) -> Void) {
        let locations = LocationDetail.availableLocations.map { detail -> Location in
            let location = Location(identifier: detail.id, display: detail.displayString)
            return location
        }
        
        let collection = INObjectCollection<Location>(items: locations)
        completion(collection, nil)
    }
    
    override func handler(for intent: INIntent) -> Any {
        // This is the default implementation.  If you want different objects to handle different intents,
        // you can override this and return the handler you want for that particular intent.
        
        return self
    }
    
}

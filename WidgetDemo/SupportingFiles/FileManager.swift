//
//  FileManager.swift
//  WidgetDemo
//
//  Created by Hoang Cuong on 10/20/21.
//

import Foundation

extension FileManager {
  static func sharedContainerURL() -> URL {
    return FileManager.default.containerURL(
      forSecurityApplicationGroupIdentifier: "group.com.vtl.demowidget"
    )!
  }
}

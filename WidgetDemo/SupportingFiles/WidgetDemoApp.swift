//
//  WidgetDemoApp.swift
//  WidgetDemo
//
//  Created by Hoang Cuong on 10/20/21.
//

import SwiftUI

@main
struct WidgetDemoApp: App {
    var body: some Scene {
        WindowGroup {
            LoginView()
        }
    }
}

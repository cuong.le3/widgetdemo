//
//  LocationDetail.swift
//  WidgetDemo
//
//  Created by Hoang Cuong on 10/22/21.
//

import Foundation

struct WeatherDetail: Codable {
    let weather: Weather
    let location: LocationDetail
}

struct LocationDetail: Codable {
    let id: String
    let displayString: String
    
    static let availableLocations = [
        LocationDetail(id: "1", displayString: "Hong Kong"),
        LocationDetail(id: "2", displayString: "Beijing"),
        LocationDetail(id: "3", displayString: "London")
    ]
}

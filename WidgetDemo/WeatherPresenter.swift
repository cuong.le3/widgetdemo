//
//  WeatherPresenter.swift
//  WidgetDemo
//
//  Created by Hoang Cuong on 10/20/21.
//

import Foundation

final class WeatherPresenter {
    let weatherList: [WeatherDetail]
    
    private let interacter: WeatherInteracter
    
    func tempStr(_ index: Int) -> String {
        "Today temparature is \(weatherList[index].weather.temp)"
    }
    
    func rainingStr(_ index: Int) -> String {
        let weather = weatherList[index].weather
        return weather.isCloud ? "Sky is cloudy" : (weather.isRaining ? "It will be raining today" : "Sky is clear")
    }
    
    func isNight(_ index: Int) -> Bool {
        let weather = weatherList[index].weather
        return weather.temp <= 20
    }
    
    func iconName(_ index: Int) -> String {
        let weather = weatherList[index].weather
        return (isNight(index) ? "night" : "day")
        +
        "_"
        +
        (weather.isCloud ? "cloud" : (weather.isRaining ? "rain" : "clear"))
    }
    
    func logout() {
        interacter.writeWeatherContentsToSharedFile(weatherList: [])
    }
    
    init() {
        self.weatherList = LocationDetail.availableLocations.map({
            WeatherDetail(weather: Weather(), location: $0)
        })
        self.interacter = WeatherInteracter()
        self.interacter.writeWeatherContentsToSharedFile(weatherList: weatherList)
    }
}

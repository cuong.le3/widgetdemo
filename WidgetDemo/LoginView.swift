//
//  LoginView.swift
//  WidgetDemo
//
//  Created by Hoang Cuong on 11/2/21.
//

import SwiftUI
import WidgetKit

struct LoginView: View {
    @State var authenticated: Bool = false
    @State var loginStr: String = ""
    
    var body: some View {
        ZStack {
            if authenticated {
                WeatherView(presenter: WeatherPresenter(), authenticated: $authenticated)
            } else {
                VStack {
                    TextField("Enter 1234", text: $loginStr)
                    Button("Login") {
                        authenticated = loginStr == "1234"
                    }
                }
                .padding(.horizontal, 30)
            }
        }
        .onReceive(NotificationCenter.default.publisher(for: UIApplication.willResignActiveNotification)) { _ in
            WidgetCenter.shared.reloadAllTimelines()
        }
    }
}
//
//struct LoginView_Previews: PreviewProvider {
//    static var previews: some View {
//        LoginView()
//    }
//}

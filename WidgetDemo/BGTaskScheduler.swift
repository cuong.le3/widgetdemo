//
//  BGTaskScheduler.swift
//  WidgetDemo
//
//  Created by Hoang Cuong on 10/29/21.
//

import UIKit
import Foundation
import BackgroundTasks
import WidgetKit

let TASK_ID_REFRESH_WEATHER = "com.sample.refresh"
let SESSION_ID_REFRESH_CALL = "com.sample.refreshCall"

class BGTaskHelper: NSObject, URLSessionDelegate {
    static let shared = BGTaskHelper()

    private lazy var urlSession: URLSession = {
        let config = URLSessionConfiguration.background(withIdentifier: SESSION_ID_REFRESH_CALL)
        config.isDiscretionary = true
        config.sessionSendsLaunchEvents = true
        return URLSession(configuration: config, delegate: self, delegateQueue: nil)
    }()

    func requestWith(strURL: String,
                     params: [String: String],
                     completion: @escaping((Data?, URLResponse?, Error?)) -> Void)
    {
        guard var urlComponents = URLComponents(string: strURL) else { return }
        urlComponents.queryItems = params.map({ URLQueryItem(name: $0.key, value: $0.value) })
        print("REQUEST URL : =============== ", strURL)
        
        var request = URLRequest(url: urlComponents.url!)
        request.httpMethod = "GET"

        let task = urlSession.dataTask(with: request) { (data, response, error) in

            guard let response = response, let data = data else {
                completion((nil, nil, error))
                return
            }

            do {
                let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String:Any]
                print("RESPONSE JSON : =============== ", json ?? "")
                if let main = json?["main"] as? [String: Any],
                   let temp = main["temp"] as? Double {
                    let weatherList = LocationDetail.availableLocations.map({ location -> WeatherDetail in
                        var weather = Weather()
                        weather.temp = Int(temp)
                        return WeatherDetail(weather: weather, location: location)
                    })
                    WeatherInteracter().writeWeatherContentsToSharedFile(weatherList: weatherList)
                    WidgetCenter.shared.reloadAllTimelines()
                }

            } catch let error {
                print("RESPONSE PARSING ERROR : =============== ", error)
            }

            completion((data, response, error))
        }
        task.resume()
    }

    func registerAutoRefreshWeather() {
        BGTaskScheduler.shared.register(forTaskWithIdentifier: TASK_ID_REFRESH_WEATHER, using: nil) { task in
            task.setTaskCompleted(success: true)
            self.handleWeatherRefresh(task: task)
        }
    }

    func scheduleWeatherRefresh() {

        BGTaskScheduler.shared.cancelAllTaskRequests()

        let request = BGProcessingTaskRequest(identifier: TASK_ID_REFRESH_WEATHER)
        // Fetch every 5 minutes
        request.earliestBeginDate = Date(timeIntervalSinceNow: 5 * 60)
        request.requiresNetworkConnectivity = true

        do {
            try BGTaskScheduler.shared.submit(request)
        } catch {
            print("Could not schedule token refresh : ", error)
        }
    }

    func handleWeatherRefresh(task: BGTask) {

        scheduleWeatherRefresh()

        let queue = OperationQueue()
        queue.maxConcurrentOperationCount = 1

        let operation = BlockOperation()
        operation.addExecutionBlock {
            let dict: [String: String] = ["q" : "London,uk",
                                          "appid" : "cf31257467a6bf307141aebd5b53ccc0"]
            BGTaskHelper.shared.requestWith(strURL: "https://api.openweathermap.org/data/2.5/weather",
                                            params: dict) {
                (data, response, error) in
                task.setTaskCompleted(success: true)
                // saving new token
            }
        }

        task.expirationHandler = {
            operation.cancel()
        }
        operation.completionBlock = {
            task.setTaskCompleted(success: !operation.isCancelled)
        }
        queue.addOperation(operation)
    }
}

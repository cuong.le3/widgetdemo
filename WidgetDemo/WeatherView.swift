//
//  ContentView.swift
//  WidgetDemo
//
//  Created by Hoang Cuong on 10/20/21.
//

import SwiftUI
import WidgetKit

struct WeatherView: View {
    let presenter: WeatherPresenter
    @State var selectedIndex: Int = 0
    @Binding var authenticated: Bool
        
    var body: some View {
        TabView(selection: $selectedIndex) {
            ForEach(presenter.weatherList.indices, id: \.self) { index in
                VStack {
                    Image(presenter.iconName(index))
                        .resizable()
                        .frame(width: 80, height: 80)
                    Text(presenter.tempStr(index))
                        .padding()
                    Text(presenter.rainingStr(index))
                        .padding()
                    Text(presenter.weatherList[index].location.displayString)
                        .padding()
                    Button("Logout") {
                        authenticated = false
                        presenter.logout()
                    }
                }
                .padding()
            }
        }
        .tabViewStyle(PageTabViewStyle())
        .onOpenURL(perform: { (url) in
            if let index = presenter.weatherList.firstIndex(where: {
                "widget-demo://\($0.location.id)" == url.absoluteString
            }) {
                self.selectedIndex = index
            }
        })
    }
}

//struct ContentView_Previews: PreviewProvider {
//    static var previews: some View {
//        ContentView()
//    }
//}

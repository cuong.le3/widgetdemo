//
//  Weather.swift
//  WidgetDemo
//
//  Created by Hoang Cuong on 10/20/21.
//

import Foundation

struct Weather: Codable {
    let temp: Int
    let isRaining: Bool
    var isCloud: Bool
    
    init() {
        temp = Int.random(in: 12...28)
        isRaining = .random()
        isCloud = .random()
    }
}

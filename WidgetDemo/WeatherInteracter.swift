//
//  WeatherInteracter.swift
//  WidgetDemo
//
//  Created by Hoang Cuong on 10/20/21.
//

import Foundation

final class WeatherInteracter {
    func writeWeatherContentsToSharedFile(weatherList: [WeatherDetail]) {
        let archiveURL = FileManager.sharedContainerURL()
          .appendingPathComponent("contents.json")
        print(">>> \(archiveURL)")
        let encoder = JSONEncoder()
        if let dataToSave = try? encoder.encode(weatherList) {
          do {
            try dataToSave.write(to: archiveURL)
          } catch {
            print("Error: Can't write contents")
            return
          }
        }
      }
}

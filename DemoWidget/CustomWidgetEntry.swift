//
//  WidgetWeather.swift
//  WidgetDemo
//
//  Created by Hoang Cuong on 10/21/21.
//

import WidgetKit
import SwiftUI

struct CustomWidgetEntry: TimelineEntry {
    let date: Date
    let location: Location
}

//
//  WidgetWeatherInteracter.swift
//  DemoWidgetExtension
//
//  Created by Hoang Cuong on 10/20/21.
//

import Foundation

final class WidgetWeatherInteracter {
    func readWeatherFromSharedFile() -> [WeatherDetail] {
        var contents: [WeatherDetail] = []
        let archiveURL =
          FileManager.sharedContainerURL()
            .appendingPathComponent("contents.json")
        print(">>> \(archiveURL)")

        let decoder = JSONDecoder()
        if let codeData = try? Data(contentsOf: archiveURL) {
          do {
            contents = try decoder.decode([WeatherDetail].self, from: codeData)
          } catch {
            print("Error: Can't decode contents")
          }
        }
        return contents
    }
}

//
//  WidgetWeatherPresenter.swift
//  DemoWidgetExtension
//
//  Created by Hoang Cuong on 10/20/21.
//

import SwiftUI

final class WidgetWeatherPresenter {
    let weatherList: [WeatherDetail]
    let isLogout: Bool
    var entry: LocationDetailProvider.Entry

    private let interacter: WidgetWeatherInteracter
    
    var mainWeatherIndex: Int {
        weatherList.firstIndex (where: { $0.location.id == entry.location.identifier }) ?? 0
    }
    
    var minorWeatherIndex: Int {
        weatherList.firstIndex (where: { $0.location.id != weatherList[mainWeatherIndex].location.id }) ?? 1
    }
    
    func tempStr(_ index: Int) -> Text {
        Text("\(weatherList[index].weather.temp)")
            .font(.title)
        +
        Text("o")
            .font(.title3)
            .baselineOffset(12.0)
        +
        Text("C")
            .font(.title)
    }
    
    func smallTempStr(_ index: Int) -> Text {
        Text("\(weatherList[index].weather.temp)")
            .font(.title3)
        +
        Text("o")
            .font(.body)
            .baselineOffset(12.0)
        +
        Text("C")
            .font(.title3)
    }
    
    func rainingStr(_ index: Int) -> String {
        let weather = weatherList[index].weather
        return weather.isCloud ? "Sky is cloudy" : (weather.isRaining ? "It will be raining today" : "Sky is clear")
    }
    
    func isNight(_ index: Int) -> Bool {
        let weather = weatherList[index].weather
        return weather.temp <= 20
    }
    
    func iconName(_ index: Int) -> String {
        let weather = weatherList[index].weather
        return (isNight(index) ? "night" : "day")
        +
        "_"
        +
        (weather.isCloud ? "cloud" : (weather.isRaining ? "rain" : "clear"))
    }
    
    init(entry: LocationDetailProvider.Entry) {
        self.interacter = WidgetWeatherInteracter()
        self.entry = entry
        self.weatherList = self.interacter.readWeatherFromSharedFile()
        self.isLogout = weatherList.isEmpty
    }
}

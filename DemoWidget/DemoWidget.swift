//
//  DemoWidget.swift
//  DemoWidget
//
//  Created by Hoang Cuong on 10/20/21.
//

import WidgetKit
import SwiftUI

extension Location {
    convenience init(detail: LocationDetail) {
        self.init(identifier: detail.id, display: detail.displayString)
    }
}

struct LocationDetailProvider: IntentTimelineProvider {
    func placeholder(in context: Context) -> CustomWidgetEntry {
        CustomWidgetEntry(date: Date(), location: Location(detail: LocationDetail.availableLocations.first!))
    }

    func getSnapshot(for configuration: SelectLocationIntent, in context: Context, completion: @escaping (CustomWidgetEntry) -> ()) {
        let entry = CustomWidgetEntry(date: Date(), location: Location(detail: LocationDetail.availableLocations.first!))
        completion(entry)
    }

    func getTimeline(for configuration: SelectLocationIntent,
                     in context: Context,
                     completion: @escaping (Timeline<CustomWidgetEntry>) -> ()) {
        var entries: [CustomWidgetEntry] = []
 
        let currentDate = Date()
        let defaultLocation = Location(detail: LocationDetail.availableLocations.first!)
        for hourOffset in 0 ..< 5 {
            let entryDate = Calendar.current.date(byAdding: .hour, value: hourOffset, to: currentDate)!
            let entry = CustomWidgetEntry(date: entryDate, location: configuration.location ?? defaultLocation)
            entries.append(entry)
        }

        let timeline = Timeline(entries: entries, policy: .atEnd)
        completion(timeline)
    }
}

@main
struct DemoWidget: Widget {
    let kind: String = "Weather Widgets"

    var body: some WidgetConfiguration {
        IntentConfiguration(kind: kind,
                            intent: SelectLocationIntent.self,
                            provider: LocationDetailProvider()) { entry in
            WidgetWeatherView(presenter: WidgetWeatherPresenter(entry: entry))
        }
        .configurationDisplayName("Weather Widget")
        .description("This widget can show the weather")
        .supportedFamilies([.systemSmall, .systemMedium])
    }
}

//struct DemoWidget_Previews: PreviewProvider {
//    static var previews: some View {
//        DemoWidgetEntryView(entry: SimpleEntry(date: Date()))
//            .previewContext(WidgetPreviewContext(family: .systemSmall))
//    }
//}

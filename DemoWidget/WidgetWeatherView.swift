//
//  WidgetWeatherView.swift
//  DemoWidgetExtension
//
//  Created by Hoang Cuong on 10/20/21.
//

import SwiftUI
import WidgetKit

extension LocationDetail {
    var url: URL {
        URL(string: "widget-demo://\(id )")!
    }
}

struct WidgetWeatherView : View {
    let presenter: WidgetWeatherPresenter
    
    @Environment(\.widgetFamily) var widgetFamily

    var body: some View {
        if presenter.isLogout {
            Text("Logout")
        } else {
            if widgetFamily == .systemSmall {
                WidgetSmallWeatherView(presenter: presenter)
            } else {
                WidgetMediumWeatherView(presenter: presenter)
            }
        }
    }
}

struct WidgetMediumWeatherView: View {
    let presenter: WidgetWeatherPresenter
    
    var body: some View {
        return VStack {
            HStack {
                Text("Weather update at ")
                    .font(.title2)
                +
                Text(presenter.entry.date, style: .time)
                    .font(.title2)
                Spacer()
            }
            Link(destination: presenter.weatherList[presenter.mainWeatherIndex].location.url) {
                HStack {
                    Text(presenter.entry.location.displayString)
                        .font(.title3)
                    Spacer()
                    presenter.smallTempStr(presenter.mainWeatherIndex)
                        .padding(.trailing, 10)
                    Image(presenter.iconName(presenter.mainWeatherIndex))
                        .resizable()
                        .frame(width: 48, height: 48)
                }
            }
            Link(destination: presenter.weatherList[presenter.minorWeatherIndex].location.url) {
                HStack {
                    Text(presenter.weatherList[presenter.minorWeatherIndex].location.displayString)
                        .font(.title3)
                    Spacer()
                    presenter.smallTempStr(presenter.minorWeatherIndex)
                        .padding(.trailing, 10)
                    Image(presenter.iconName(presenter.minorWeatherIndex))
                        .resizable()
                        .frame(width: 48, height: 48)
                }
            }
        }
        .padding()
    }
}

struct WidgetSmallWeatherView: View {
    let presenter: WidgetWeatherPresenter
    
    var body: some View {
        VStack {
            HStack {
                Image(presenter.iconName(presenter.mainWeatherIndex))
                    .resizable()
                    .frame(width: 48, height: 48)
                presenter.tempStr(presenter.mainWeatherIndex)
            }
            Text(presenter.rainingStr(presenter.mainWeatherIndex))
            HStack {
                Spacer()
                Text(presenter.entry.date, style: .time)
                    .font(.caption)
            }
            HStack {
                Spacer()
                Text(presenter.entry.location.displayString)
                    .font(.caption2)
            }
        }
        .padding()
        .widgetURL(presenter.weatherList[presenter.mainWeatherIndex].location.url)
    }
}
